{
  const __babel_path = ARGV[0];

  if (imports.searchPath.indexOf(__babel_path) === -1) {
    imports.searchPath.unshift(__babel_path);
  }

  log(JSON.stringify(ARGV));

  const Babel = imports.babel.Babel;
  const GjsBabelFileUtil = imports.files.GjsBabelFileUtil;

  function gjsImport() {
    return {
      visitor: {
        MemberExpression(path) {
          let importNode = path.node;
          let object = importNode.object;
          let property = importNode.property;

          if (object.type === 'Identifier' && object.name === 'imports' && importNode.property !== null && !importNode.property.name.startsWith('babel$$')) {

       if(Array.from(ARGV).slice(3).includes(path.node.property.name)) {
            path.node.property.name = `babel$$${path.node.property.name}`
            }
           /* path.replaceWith({
              type: 'MemberExpression',
              object: {
                type: 'MemberExpression',
                object: object,
                property: {
                  type: 'Identifier',
                  name: 'babel'
                },
                computed: false
              },
              property: property,
              computed: false
            });*/
          }
        }
      }
    };
  }

  function gjsExport() {
    return {
      visitor: {
        ExportNamedDeclaration(path) {
          let exportNode = path.node;
          let expr = exportNode.declaration;
          if (expr.type === 'ClassDeclaration') {
            expr.type = expr.type.replace('Declaration', 'Expression');
            path.replaceWith({
              type: 'VariableDeclaration',
              kind: 'var',
              declarations: [
                {
                  type: 'VariableDeclarator',
                  id: expr.id,
                  init: expr
                }
              ]
            });
            // type, declaration, specifiers, source
          }
        }
      }
    };
  }
  Babel.registerPlugin('gjs-export', gjsExport);
  Babel.registerPlugin('gjs-import', gjsImport);

  GjsBabelFileUtil.write_to_file(
    ARGV[2],
    Babel.transform(GjsBabelFileUtil.read_file(ARGV[1]), {
      plugins: [
        'transform-function-bind',
        'transform-decorators-legacy',
        'transform-class-properties',
        'gjs-export',
        'gjs-import'
      ]
    }).code
  );
  const file = GjsBabelFileUtil.file(ARGV[2]).get_basename();
  const dir = GjsBabelFileUtil.file(ARGV[2]).get_parent().get_path();
}
