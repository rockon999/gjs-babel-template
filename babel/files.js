var GjsBabelFileUtil = {};

{
  const { GLib, Gio } = imports.gi;

  const PERMISSIONS_MODE = parseInt('0744', 8);

  GjsBabelFileUtil.write_to_file = function(file_path, text) {
    try {
      let file = Gio.file_new_for_path(file_path);
      let parent = file.get_parent();
      if (GLib.mkdir_with_parents(parent.get_path(), PERMISSIONS_MODE) === 0) {
        let [success] = file.replace_contents(
          text,
          null,
          false,
          Gio.FileCreateFlags.REPLACE_DESTINATION,
          null
        );
        return success;
      }
    } catch (error) {
      log('Error writing to file: ' + file_path);
      log(error);
    }
    return false;
  };

  GjsBabelFileUtil.file = function(file_path) {
    try {
      let file = Gio.file_new_for_path(file_path);
      return file;
    } catch (error) {
      log('Error finding file: ' + file_path);
      log(error);
    }
    return false;
  };

  GjsBabelFileUtil.read_file = function(file_path) {
    try {
      let file = Gio.file_new_for_path(file_path);
      let [success, contents] = file.load_contents(null);
      if (success) {
        return String(contents);
      } else {
        return '';
      }
    } catch (error) {
      log('Error reading file: ' + file_path);
      log(error);
    }
    return '';
  };
}
